import java.awt.Color;
import java.awt.Point;

/**
 * A class that represents a point (in 2 dimensions) with a given color (using
 * RGB values).
 * 
 * ********This class is fully implemented.******************
 * 
 * @author Alayna Ruberg
 * @author modified by Chuck Cusack, January 2013, February 2015
 * 
 */
public class ColoredPoint {
	private int x;
	private int y;
	private int red;
	private int green;
	private int blue;

	public ColoredPoint(int x, int y, int r, int g, int b) {
		this.x = x;
		this.y = y;
		red = r;
		green = g;
		blue = b;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Point getPoint() {
		return new Point(x, y);
	}

	public Color getColor() {
		return new Color(red, green, blue);
	}

	public int getR() {
		return red;
	}

	public int getG() {
		return green;
	}

	public int getB() {
		return blue;
	}

	// A string representation of the point in the format:
	// x,y:red green blue
	// For instance:
	// 25,75:0 127 255
	//
	@Override
	public String toString() {
		return getX() + "," + getY() + ":" + getR() + " " + getG() + " "
				+ getB();
	}
}
